console.log("Implement servermu disini yak 😝!");

const http = require('http');
const fs = require('fs');
const path = require('path');
const PUBLIC_DIRECTORY = path.join(__dirname, '../public');

const portName = 'localhost';

// get port from environment variable
const { PORT = 8000 } = process.env;

const getHTML = (pathName, result, statusCode) => {
    let filePath = PUBLIC_DIRECTORY + pathName;
    return fs.readFile(filePath, 'utf-8', function (err, html) {
        if (statusCode === 200) {
            result.writeHead(200, { "Content-Type": "text/html" });
        }
        else {
            result.writeHead(404, { "Content-Type": "text/html" });
        }
        result.end(html);
    })
}

const openFileStream = (pathName, result, fileType) => {
    let filePath = path.join(PUBLIC_DIRECTORY, pathName);
    let fileStream;
    if (fileType === 'css' || fileType === 'javascript') {
        fileStream = fs.createReadStream(filePath, "UTF-8");
        result.writeHead(200, { "Content-Type": `text/${fileType}` });
    } else {
        fileStream = fs.createReadStream(filePath);
        result.writeHead(200, { "Content-Type": `image/${fileType}` });
    }
    fileStream.pipe(result);
}

http.createServer((req, res) => {
    if (req.url === "/" || req.url === "/index.html") {
        getHTML('/index.html', res, 200);
    }
    else if (req.url === "/cars" || req.url === "/cars.html") {
        getHTML('/cars.html', res, 200);
    }
    else if (req.url.match("\.css$")) {
        openFileStream(req.url, res, 'css');
    }
    else if (req.url.match("\.js$")) {
        openFileStream(req.url, res, 'javascript');
    }
    else if (req.url.match("\.jpg$")) {
        openFileStream(req.url, res, 'jpg');
    }
    else if (req.url.match("\.png$")) {
        openFileStream(req.url, res, 'png');
    }
    else if (req.url.match("\.svg$")) {
        openFileStream(req.url, res, 'svg+xml');
    }
    else {
        getHTML('/404.html', res, 404);
    }
}).listen(PORT, portName, () => {
    console.log(`Server is running on server http://${portName}:${PORT}`);
})